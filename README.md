# ASE interface for QSTEM

QSTEM is a suite of software for quantitative image simulation in electron microscopy, including TEM, STEM and CBED image simulation. 

This project interfaces the QSTEM code with Python and the Atomic Simulation Environment (ASE) to provide a single seamless environment for building models, simulating and analysing images

QSTEM webpage: http://qstem.org/

ASE webpage: http://wiki.fysik.dtu.dk/ase

## Dependencies

* [Python](http://www.python.org/) 3.4-3.6
* [ASE](http://wiki.fysik.dtu.dk/ase)
* [NumPy](http://docs.scipy.org/doc/numpy/reference/)

Optional:
* [SciPy](https://www.scipy.org/)
* [Jupyter and IPython](http://jupyter.org/)
* [scikit-image](http://scikit-image.org/)
* [matplotlib](http://matplotlib.org/)
* [PIL (Python Image Library)](http://www.pythonware.com/products/pil/)

## Installation

Install QSTEM by following the instructions on the webpage:

For Windows: http://qstem.org/?page_id=25

For Linux or OSX: https://github.com/QSTEM/QSTEM/wiki/Building-from-source

After installing QSTEM, the program `stem3` should be callable from the command line.  
To test this open a command line and write `stem3`, you should get the following output: 
```
could not open input file stem.dat
```
If instead `stem3` is unrecognized, ensure that it is on your `PATH` system variable.

---

If you are new to Python the easiest way to get started is downloading a bundle installer.  
We recommend [Anaconda](https://www.continuum.io/downloads), this platform includes Python and all the dependencies (including the optional ones) except for ASE.

After installing Anaconda open the `Anaconda prompt` and write
```
pip install ase
```
to install ASE and
```
pip install qstem-ase
```
to install the QSTEM interface to ASE. 

QSTEM is now ready to be used from Python. We recommend that you start by testing one of the interactive notebooks included under examples.

To download the examples in the current directory and open jupyter you can write
```
git clone https://gitlab.com/jamad/qstem-ase.git
cd qstem-ase/examples
jupyter notebook
```
a navigator will show up and you can open the notebooks.

## Example
Set up a graphite model and simulate a basic HRTEM image:

```python
from qstem import Multislice, CTF
from ase.lattice.hexagonal import Graphite

a,c=2.46,6.70 # lattice constants
directions=[[1,-2,1,0],[2,0,-2,0],[0,0,0,1]] # a right-angled unit cell
atoms = Graphite(symbol='C', latticeconstant={'a':a,'c':c}, directions=directions, size=(10,5,5))

v0=300 # acceleration voltage in keV
exit_wave = Multislice(atoms, energy=energy).run() # the exit wavefunction

ctf = CTF(defocus = 100, Cs = 5*10**4, focal_spread=29)
img_wave = wave.apply_ctf(ctf) # the wavefunction at the detector plane

img_intensity = img_wave.detect() # image intensity
```